# to be included from singlepage/Makefile and multipage/Makefile

PACKAGE=bgipc
BASE=../../..
SRC=$(BASE)/$(PACKAGE).xml
CSS=$(BASE)/$(PACKAGE).css
VALIDFILE=$(BASE)/$(PACKAGE).valid
BINPATH=$(BASE)/bin
LIBPATH=$(BASE)/lib
IMGPATH=$(BASE)/images
IMGS=pipe1-96-4.149.png
HEADER="Beej's Guide to Unix IPC"

PYTHONPATH=./lib:$(LIBPATH)
export PYTHONPATH

